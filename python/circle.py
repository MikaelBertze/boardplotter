import printercom as printercom
import serialsetup
from printer.printer import Printer
from printer.storage import PersistantStorage

import math


#printercom.sendAndWait(None, "hej")
s = PersistantStorage("./storage.data")
print "current"
print s.get()

p = Printer(printercom, serialsetup.startSerial(), s)


cx, cy = (1000,800)
radius = 100
xes = []
yes = []
for laps in range(1):
    for i in xrange(0,360,2):
        print i
        x = cx + radius * math.cos(math.radians(i))
        y = cy + radius * math.sin(math.radians(i))
        xes.append(x)
        yes.append(y)
        print (x,y)
        p.goto_xy(x,y)

