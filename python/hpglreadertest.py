from printer.hpgl_reader import HpglReader

reader = HpglReader()

paths = []
max_x = 0
max_y = 0
#for p in reader.parse(r"/home/micke/Pictures/dickbut2.hpgl"):
for p in reader.parse(r"./60016.hpgl"):
    xmax = max([x[0] for x in p])
    ymax = max([x[1] for x in p])

    max_x = max(max_x, xmax)
    max_y = max(max_y, ymax)
    paths.append(p)

print len(paths)
print max_x
print max_y


width = 1000.0
height = 500.0

q1 = width / max_x
q2 = height / max_y

q = min(q1,q2)

corner = (600,1200)

print q

def getCoord(a,b, q):
    print a
    print b
    c =  ((a[0] + b[0]*q), (a[1] + -b[1] * q))
    print c
    return c

import printercom as printercom
import serialsetup
from printer.printer import Printer
from printer.storage import PersistantStorage

s = PersistantStorage("./storage.data")

p = Printer(printercom, serialsetup.startSerial(), s)

for path in paths:
    p.pen(0)
    coord = getCoord(corner, path[0], q)
    print "path"
    p.goto_xy(coord[0], coord[1], 2)

    
    for point in path:
        p.pen(1)
        coord = getCoord(corner, point, q)
        print coord
        p.goto_xy(coord[0], coord[1], 7)
    p.go()

p.pen(0)
p.goto_xy(1000,800, 1)
p.go()
