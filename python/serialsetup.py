import serial

def startSerial():
    # configure the serial connections (the parameters differs on the device you are connecting to)
    return serial.Serial(port='/dev/ttyUSB0', baudrate=115200, timeout=1)
