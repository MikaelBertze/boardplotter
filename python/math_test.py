import printercom as printercom
import serialsetup
from printer.printer import Printer
from printer.storage import PersistantStorage
from printer.printer_math import PrinterMath


s = PersistantStorage("./storage.data")
p = Printer(printercom, serialsetup.startSerial(), s)

m = PrinterMath()

p.pen(False)

p.goto_xy(500,800,1)

p.pen(True)

s_line = m.line((500,800), (1200,900))

for x,y in s_line:
    p.goto_xy(x,y,3)

p.go()

