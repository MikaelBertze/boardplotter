import printercom as printercom
import serialsetup
from printer.printer import Printer
from printer.storage import PersistantStorage

import time

s = PersistantStorage("./storage.data")

#s.set(1000,1000)
p = Printer(printercom, serialsetup.startSerial(), s)

size = int(raw_input("size:"))

p.move_ab(0,0,5,1) # pen down
time.sleep(5)

p.move_ab(size, 0, 5, 1)
p.move_ab(0, size, 5, 1)
p.move_ab(-size, 0, 5, 1)
p.move_ab(0, -size, 5, 1)


