
import json
    
def getLine(ser):
    return ser.readline().strip()

def sendAndWaitForReady(ser, command):
    send(ser, command)
    wait_for_ready(ser)

def sendAndWaitForMore(ser, command):
    send(ser, command)
    wait_for_more(ser)

def send(ser, command):
    print "sending: " + command
    ser.write(command)

def kick(ser):
    ser.read_all()
    send(ser, "KICK!:;")
    wait_for_ready(ser)

def wait_for_more(ser):
    print "wait for more"
    while True:
        line = ser.readline().strip()
        #lines = ser.readlines()
        if line:
        #if len(lines) > 0:
            print line
            # for l in lines:
            #     print l
            # line = lines[-1]
            if line == "<":
                print "more recieved"
                return
        #print "retry..."

def wait_for_ready(ser):
    print "wait for ready"
    while True:
        line = ser.readline().strip()
        #lines = ser.readlines()
        if line:
        #if len(lines) > 0:
            print line
            # for l in lines:
            #     print l
            # line = lines[-1]
            if line == ">":
                print "returning"
                return
        #print "retry..."

def get_status(ser):
    send(ser, "STATUS:;")
    data = getLine(ser)
    print data
    status = json.loads(data)["data"]
    wait_for_ready(ser)
    return status

def setParameter(ser, p, v):
    command = "SET:{}{}".format(p,v)
    send(ser, command)
    wait_for_ready(ser)

def move(ser, a, b):
    command = "MOVE:{},{}".format(a,b)
    send(ser, command)
    wait_for_ready(ser)

def write(ser, text):
    command = "DRAW_STRING:{}".format(text)
    send(ser, command)
    wait_for_ready(ser)


def draw(ser, x, y, pen):
    command = "DRAW:{},{},{};".format(x,y,pen)
    send(ser, command)
    wait_for_ready(ser)

def circ(ser, start_ang, stop_ang, radius):
    command = "DRAW_CIRC:{},{},{}".format(start_ang, stop_ang, radius)
    send(ser, command)
    wait_for_ready(ser)

def rel_draw(ser, rel_x, rel_y, pen, status = None):
    if not status:
        status = get_status(ser)
        print status
    X = int(status["X"])
    Y = int(status["Y"])
    draw(ser, X + rel_x, Y + rel_y, pen)


def rel_draw_straight(ser, rel_x, rel_y, pen, status = None):
    if not status:
        status = get_status(ser)
    X = status["X"]
    Y = status["Y"]
    if rel_x != 0:
        k = rel_y / rel_x

    for x in range(0, rel_x, 10):
        draw(ser, X + x, Y + (rel_x * k), pen)
    draw(ser, X + rel_x, Y + rel_y, pen)

