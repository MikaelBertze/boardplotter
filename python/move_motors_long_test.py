import serialsetup
import json
import printercom
        
ser = serialsetup.startSerial()
printercom.kick(ser)

while True:
    printercom.move(ser, 1, 1)
    printercom.move(ser, -1, -1)
    