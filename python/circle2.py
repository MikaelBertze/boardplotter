import printercom as printercom
import serialsetup
from printer.printer import Printer
from printer.storage import PersistantStorage
from printer.printer_math import PrinterMath


s = PersistantStorage("./storage.data")
p = Printer(printercom, serialsetup.startSerial(), s)

m = PrinterMath()

points = m.circle((800,800),100, 0, 360)

first_point = points.pop(0)
p.pen(False)
p.goto_xy(first_point[0], first_point[1], 1)
p.pen(True)

for i in range(3):
    for point in points:
        p.goto_xy(point[0], point[1], 3)

p.pen(False)
p.goto_xy(1000,800,1)

p.go()


print points
