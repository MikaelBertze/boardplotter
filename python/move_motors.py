import printercom as printercom
import serialsetup
from printer.printer import Printer
from printer.storage import PersistantStorage


s = PersistantStorage("./storage.data")
p = Printer(printercom, serialsetup.startSerial(), s)

# p.pen(True)
# for x in range(10):
#     for i in range(5):   
#         p.move_ab(697.0,-970.0, 4)
#         p.move_ab(-428.0,-464.0, 4)
#         p.move_ab(-403.0,385.0, 4)
#         p.move_ab(403.0,443.0, 4)
#         p.move_ab(428.0,-364.0, 4)
#         p.move_ab(773.0,973.0, 4)
#         p.move_ab(332.0,504.0, 4)
#         p.move_ab(-506.0,308.0, 4)
#         p.move_ab(-307.0,-487.0, 4)
#         p.move_ab(481.0,-325.0, 4)
#         p.move_ab(-1470.0,-3.0, 4)

for i in range(10):
    p.pen(True)
    p.move_ab(1000,100,3)
    p.move_ab(100,1000,3)
    p.move_ab(-1000,-100,3)
    p.move_ab(-100,-1000,3)
    p.go()
    p.pen(False)
   