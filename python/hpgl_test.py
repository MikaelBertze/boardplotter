from printer.hpgl_reader import HpglReader

import printercom as printercom
import serialsetup
from printer.printer import Printer
from printer.storage import PersistantStorage


def dataToTuples(data):
    if data != '':
        print data
        x = [float(i) for i in data.split(",")]
        if len(x) > 0:
            assert len(x) % 2 == 0, "strange..."
            return zip(x[0::2], x[1::2])

def normalize(coord):
    pass


s = PersistantStorage("./storage.data")
p = Printer(printercom, serialsetup.startSerial(), s)


reader = HpglReader()

commands = reader.parse(r"/home/micke/Pictures/box.hpgl")

instructions = []

for c in commands:
    command = c[0:2]
    data = c[2:]
    print command
    print data
    if command == "PU" or command == "PD":
        instructions.append((command, dataToTuples(data)))


maxx = 0
maxy = 0
pen = False
for i in instructions:
    #coords = i[1]
    #maxx = max(maxx, max([x[0] for x in coords]))
    #maxy = max(maxy, max([x[1] for x in coords]))
    if i[0] == "PU" and pen:
        p.pen(False)
    
    if i[0] == "PD" and not pen:
        p.pen(True)
    
    for c in i[1]:
        p.goto_xy(c[0], c[1], 4)
    

p.go()




