from printer.hpgl_reader import HpglReader

import printercom as printercom
import serialsetup
from printer.printer import Printer
from printer.storage import PersistantStorage


corner_x, corner_y = (400,1100)

import csv
with open(r"./csv/abc.csv", 'r') as csvfile:
    csv_in=csv.reader(csvfile, quoting=csv.QUOTE_NONNUMERIC)
    data = list(csv_in)

len(data)

cc = []
for i in range(0,len(data),2):
    xx = data[i]
    yy = data[i+1]
    cc.append((xx,yy))



s = PersistantStorage("./storage.data")
plotter = Printer(printercom, serialsetup.startSerial(), s)

plotter.pen(False)

for path in [zip(p[0], p[1]) for p in cc]:
    x,y = path[0]
    plotter.pen(False)
    plotter.goto_xy(x + corner_x, corner_y - y, 1)
    plotter.pen(True)
    for x,y in path:
        plotter.goto_xy(x + corner_x, corner_y - y, 1)
    
plotter.go()


    