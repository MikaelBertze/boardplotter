import math

class Printer:
    d_pulley = 1880 # mm distance between line pulleys
    a_steps_per_mm = -5.88
    b_steps_per_mm = 5.88
    def __init__(self, printercom, ser, persistant_storage_file):
        self.persistant_storage_file = persistant_storage_file
        self.printercom = printercom
        self.ser = ser
        self.commands = []
        printercom.sendAndWaitForReady(self.ser, "KICK;")

        with open("out.csv", "w") as myfile:
            myfile.write("{},{},{},{},{},{},{},{},{},{}\n".format("a","b","current_a", "current_b", "a_move", "b_move", "a_steps", "b_steps", "new_a", "new_b"))
        
    def commandsWaiting(self):
        return len(self.commands) > 0

    def go(self):
        commands = self.commands
        self.commands = []

        self.printercom.sendAndWaitForReady(self.ser, "ENABLE;")
        self.printercom.sendAndWaitForMore(self.ser, "DO;")
        i = 0
        for command in commands:
            self.printercom.sendAndWaitForMore(self.ser, command)
            i += 1
            if i == 199:
                print "MAX"
                self.printercom.sendAndWaitForReady(self.ser, "END;")
                self.printercom.sendAndWaitForMore(self.ser, "DO;")
                i = 0

        self.printercom.sendAndWaitForReady(self.ser, "END;")
        self.printercom.sendAndWaitForMore(self.ser, "DISABLE;")
                
        

        

    def set_ab(self, a, b, speed):
        current_a, current_b = self.persistant_storage_file.get()
        a_move = a - current_a
        b_move = b - current_b
        a_steps = round(a_move * Printer.a_steps_per_mm)
        b_steps = round(b_move * Printer.b_steps_per_mm)

        self.move_ab(a_steps, b_steps, speed)
        
        new_a = current_a + a_steps/Printer.a_steps_per_mm
        new_b = current_b + b_steps/Printer.b_steps_per_mm
        
        with open("out.csv", "a") as myfile:
            myfile.write("{},{},{},{},{},{},{},{},{},{}\n".format(a,b,current_a, current_b, a_move, b_move, a_steps, b_steps, new_a, new_b))
        
        self.persistant_storage_file.set(new_a, new_b)

    def pen(self, pen):
        if pen == 0:
          self.commands.append("PEN0;")
        elif pen == 1:
          self.commands.append("PEN1;")
        else:
          self.commands.append("PEN2;")
    
    def move_ab(self, a_steps, b_steps, speed):
        #assert a_steps.is_integer(), "a_steps is not an integer"
        #assert b_steps.is_integer(), "a_steps is not an integer"
        command = "M{:.0f},{:.0f},{};".format(a_steps, b_steps, speed)
        self.commands.append(command)
        
    def goto_xy(self, x, y, speed):
        new_a = self.calc_a(x,y)
        new_b = self.calc_b(x, y)
        self.set_ab(new_a, new_b, speed)
    
    def calc_a(self, x, y):
        temp = x * x + y * y
        return math.sqrt(temp)

    def calc_b(self, x, y):
        xx = Printer.d_pulley - x
        temp = xx * xx + y * y
        return math.sqrt(temp)
