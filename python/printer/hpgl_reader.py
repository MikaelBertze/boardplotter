
class HpglReader:

    def parse(self, filename):
        with open(filename) as file:
            data = file.read()
        commands = data.split(";")
        for c in [x[2:] for x in commands if x[:2] == "PD"]:
            cc = [int(x) for x in c.split(",")]
            x = cc[0::2]
            y = cc[1::2]
            yield zip(x,y)

