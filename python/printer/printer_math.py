import math
class PrinterMath:

    def line(self, start, end, dist_per_point=20):
        x1,y1 = (float(start[0]), float(start[1]))
        x2,y2 = (float(end[0]), float(end[1]))
        points = []
        length = math.sqrt((x1-x2) * (x1-x2) + (y1-y2) * (y1-y2))
        print "lenght: {}".format(length)

        num_points = length / dist_per_point
        print "num_points: {}".format(num_points)
        x_step = (x2 - x1) / num_points
        y_step = (y2 - y1) / num_points 
        for i in range(int(num_points)):
            points.append((x1 + i * x_step, y1 + i * y_step))
        
        points.append((x2,y2))
        return points

    def circle(self, center, radius, start_ang, stop_ang, points_per_lap=30):
        points = []
        deg_per_point = 360 / points_per_lap
        num_points = (stop_ang - start_ang) / deg_per_point

        for i in range(num_points):
            ang = start_ang + i*deg_per_point
            x = center[0] + radius * math.cos(math.radians(ang))
            y = center[1] + radius * math.sin(math.radians(ang))
            points.append((x,y))
            print ang

        x = center[0] + radius * math.cos(math.radians(stop_ang))
        y = center[1] + radius * math.sin(math.radians(stop_ang))
        points.append((x,y))

        return points






