
import json
import os

class PersistantStorage:

    def __init__(self, filename):
        self.filename = filename
        self.data = {}
        if os.path.isfile(self.filename):
            with open(self.filename) as data_file:    
                self.data = json.load(data_file)
        else:
            self.set(1070, 1070)

    def set(self, a, b):
        self.data["a"] = a
        self.data["b"] = b
        with open(self.filename, 'w') as outfile:
            json.dump(self.data, outfile)

    def get(self):
        return (self.data["a"], self.data["b"])
