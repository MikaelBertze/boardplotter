import cv2
import numpy as np

import printercom as printercom
import serialsetup
from printer.printer import Printer
from printer.storage import PersistantStorage
import threading


s = PersistantStorage("./storage.data")
p = Printer(printercom, serialsetup.startSerial(), s)

pen = False
lastPoint = None
corner =(300,400)
go_thread = None

def clear():
    global img
    img = np.zeros((800,1200,3), np.uint8)

def addPoint(x,y):
    global pen, lastPoint
    if pen:
        if lastPoint:
            print "drawing line"
            cv2.line(img,lastPoint,(x,y),(255,0,0),1)
        lastPoint = (x,y)


    x += corner[0]
    y += corner[1]
    print "Adding point: {},{}".format(x,y)
    p.goto_xy(x,y, 4 if pen else 1)

def togglePen():
    global pen, lastPoint
    pen =  not pen
    print "Pen set to {}".format(pen)
    p.pen(pen)
    if not pen:
        lastPoint = None

def go_done():
    print "thread done"
    if p.commandsWaiting():
        go(True)
    
    
def go(force = False):
    global go_thread
    if go_thread == None or not go_thread.isAlive() or force:
        print "Starting thread"
        go_thread = threading.Thread(target=draw, args=(p,go_done))
        go_thread.start()
    else:
        print "Could not start thread"

def draw(printer, cb):
    printer.go()
    cb()


# mouse callback function
def mouse_cb(event,x,y,flags,param):
    if event == cv2.EVENT_LBUTTONDOWN:
        addPoint(x,y)
    if event == cv2.EVENT_MBUTTONDOWN:
        addPoint(x,y)
        togglePen()
        addPoint(x,y)
        
        
        
# Create a black image, a window and bind the function to window
img = np.zeros((800,1200,3), np.uint8)
cv2.namedWindow('image')
cv2.setMouseCallback('image',mouse_cb)

while(1):
    cv2.imshow('image',img)
    
    key = cv2.waitKey(50)
    if key == ord('p'):
        togglePen()
    if key == ord('g'):
        go()
    
    if key == ord('c'):
        clear()
    
    if key == ord('q'):
        break
cv2.destroyAllWindows()
