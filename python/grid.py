import printercom as printercom
import serialsetup
from printer.printer import Printer
from printer.storage import PersistantStorage
from printer.printer_math import PrinterMath


s = PersistantStorage("./storage.data")
p = Printer(printercom, serialsetup.startSerial(), s)

m = PrinterMath()

corner_x = int(raw_input("X-corner(mm):"))
corner_y = int(raw_input("Y-corner(mm):"))
size_x = int(raw_input("X-size(mm):"))
size_y = int(raw_input("Y-size(mm):"))
rows = int(raw_input("Rows:"))
cols = int(raw_input("Cols:"))

y_step = size_y/rows
x_step = size_x/cols

k = 1

p.pen(False)
p.goto_xy(corner_x, corner_y, 1)

#draw box

corner1 = (corner_x, corner_y)
corner2 = (corner_x + size_x, corner_y)
corner3 = (corner_x + size_x, corner_y + size_y)
corner4 = (corner_x, corner_y + size_y)
lines = [m.line(corner1, corner2), m.line(corner2, corner3), m.line(corner3, corner4), m.line(corner4, corner1)]

for line in lines:
    for point in line:
        p.goto_xy(point[0], point[1], 4)
        p.pen(True)


row_dist = float(size_y) / rows
col_dist = float(size_x) / cols

for r in range(rows-1):
    y = corner_y + row_dist * (r + 1)
    p.pen(False)
    if r%2 == 0:
        line = m.line((corner_x, y), (corner_x + size_x, y))
    else:
        line = m.line((corner_x + size_x, y), (corner_x, y))
    first_point = line.pop(0)
    p.goto_xy(first_point[0], first_point[1], 1)
    p.pen(True)
    for point in line:
        p.goto_xy(point[0], point[1], 4)


for c in range(cols-1):
    x = corner_x + col_dist * (c + 1)
    p.pen(False)
    if c%2 == 0:
        line = m.line((x, corner_y), (x, corner_y + size_y))
    else:
        line = m.line((x, corner_y + size_y), (x, corner_y))
    #print line
    first_point = line.pop(0)
    p.goto_xy(first_point[0], first_point[1], 1)
    p.pen(True)
    for point in line:
        p.goto_xy(point[0], point[1], 4)



p.go()
    




