import printercom as printercom
import serialsetup
from printer.printer import Printer
from printer.storage import PersistantStorage

import math
import matplotlib.pyplot as plt


#printercom.sendAndWait(None, "hej")
s = PersistantStorage("./storage.data")
print "current"
print s.get()

p = Printer(printercom, serialsetup.startSerial(), s)


startx = 500
stopx = 1200
height = 110
centery = 800

xstep = (stopx - startx) / (360.0)

for i in range(100):
    p.pen(False)
    x = startx

    p.goto_xy(startx, centery,1)
    a = 0
    p.pen(True)
    while x < stopx:
        y = centery + height * math.sin(math.radians(a))
        p.goto_xy(x,y,4)
        x += xstep
        a += 2
    p.pen(False)
    y = centery + height * math.cos(math.radians(a))
    p.goto_xy(x,y,1)
    p.pen(True)
    while x > startx:
        y = centery + height * math.cos(math.radians(a))
        p.goto_xy(x,y,4)
        x -= xstep
        a -= 2


    p.go()
    
    
    
    

# plt.plot(xes,yes)
# plt.axis('equal')
# plt.axis([0,2000,0,2000])
# plt.show()