# BOARD PLOTTER #

## Hardware ##
1 Arduino
2 Stepper motors with driver (28BYJ-48 Stepper Motor with ULN2003)
1 Servo

![2017-05-09 22.18.05.jpg](https://bitbucket.org/repo/9pKMRdK/images/272571839-2017-05-09%2022.18.05.jpg)

![2017-05-09 21.09.42.jpg](https://bitbucket.org/repo/9pKMRdK/images/2903341190-2017-05-09%2021.09.42.jpg "Plotter head unit")

Plotter head unit

![2017-05-09 21.09.08.jpg](https://bitbucket.org/repo/9pKMRdK/images/1284808014-2017-05-09%2021.09.08.jpg "Plotter drive unit")

Plotter drive unit