#include "driver.h"

namespace plotter_drivers
{

  easy_driver::driver(motor_conf motor1, motor_conf motor2, servo_conf servo1) {
    // Record pin numbers in the inputPins array
    this->motor1 = motor1;
    this->motor2 = motor2;
    this->servo1 = servo1;

    this->setupMotors();
    this->setupServod();
  }

  void easy_driver::enable() {
    this->disableMotor(this->motor1);
    this->disableMotor(this->motor2);
  }

  void easy_driver::disable() {
    this->enableMotor(this->motor1);
    this->enableMotor(this->motor2);
  }

  void easy_driver::setStepSpeed(int stepSleep) {
    this->stepSleep = stepSleep;
  }

  void easy_driver::step(int motor1, int motor2) {
    int moves1 = 0;
    int moves2 = 0;

    float motor1divider = 1.0;
    float motor2divider = 1.0;
    int step[2] = {0,0};
    bool dir1 = motor1 > 0;
    bool dir2 = motor2 > 0;
    this->setDirection(this->motor1, dir1);
    this->setDirection(this->motor2, dir2);
    delay(1);

    int motor2steps = abs(motor2);
    int motor1steps = abs(motor1);

    int numSteps = motor1steps;

    if (motor1steps < motor2steps) {
      motor1divider = (float)motor1steps / motor2steps;
      numSteps = motor2steps;
    }

    if (motor2steps < motor1steps) {
      motor2divider = (float)motor2steps / motor1steps;
      numSteps = motor1steps;
    }
    
    float motor1current = .5;
    float motor2current = .5;

    for (int i = 0 ; i < numSteps; i++) {
      
       int current1 = (int)motor1current;
       int current2 = (int)motor2current;
      
       motor1current += motor1divider;
       motor2current += motor2divider;

       bool move1 = (int)motor1current != current1;
       bool move2 = (int)motor2current != current2;
      
       if (move1 > motor1steps)
        move1 = false;
       if (move2 > motor2steps)
        move1 = false;
      
       this->stepOne(move1, move2);
       moves1 += move1 ? 1 : 0;
       moves2 += move2 ? 1 : 0;
    }

    while (moves1 < motor1steps) {
      this->stepOne(true, false);
      moves1++;
    }

    while (moves2 < motor2steps) {
      this->stepOne(false, true);
      moves2++;
    }
  }   

  void easy_driver::setupMotors() {
    pinMode(motor1.pin_STP, OUTPUT);
    pinMode(motor1.pin_DIR, OUTPUT);
    pinMode(motor1.pin_MS1, OUTPUT);
    pinMode(motor1.pin_MS2, OUTPUT);
    pinMode(motor1.pin_EN, OUTPUT);
    
    pinMode(motor2.pin_STP, OUTPUT);
    pinMode(motor2.pin_DIR, OUTPUT);
    pinMode(motor2.pin_MS1, OUTPUT);
    pinMode(motor2.pin_MS2, OUTPUT);
    pinMode(motor2.pin_EN, OUTPUT);
    
    this->reset(motor1);
    this->reset(motor2);
  }

  void easy_driver::setupServos() {

  }
  void easy_driver::reset(motor_conf &motor) {
    digitalWrite(motor.pin_STP, LOW);
    digitalWrite(motor.pin_DIR, LOW);
    digitalWrite(motor.pin_MS1, LOW);
    digitalWrite(motor.pin_MS2, LOW);
    digitalWrite(motor.pin_EN, HIGH);
  }

  void easy_driver::enableMotor(motor_conf &motor) {
    digitalWrite(motor.pin_EN, LOW);
  }

  void easy_driver::disableMotor(motor_conf &motor) {
    digitalWrite(motor.pin_EN, HIGH);
  }
  
  void easy_driver::stepOne(bool motor1, bool motor2) {
    digitalWrite(this->motor1.pin_STP, motor1 ? HIGH : LOW);
    digitalWrite(this->motor2.pin_STP, motor2 ? HIGH : LOW);
    delay(1);
    digitalWrite(this->motor1.pin_STP, LOW);
    digitalWrite(this->motor2.pin_STP, LOW);
    delay(this->stepSleep);
  }

  void easy_driver::setDirection(motor_conf &motor, bool CW) {
    digitalWrite(motor.pin_DIR, CW ? HIGH : LOW);
  }
}
