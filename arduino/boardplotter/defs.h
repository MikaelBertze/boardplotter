
struct comdef {
  byte command;
  int data1;
  int data2;
  byte data3;
};

struct servo_conf {
  byte pin;
  byte pen1;
  byte pen2;
  byte idle;
};

struct motor_conf {
  byte pin_STP;
  byte pin_DIR;
  byte pin_MS1;
  byte pin_MS2;
  byte pin_EN;
  bool positive_cw;
};

