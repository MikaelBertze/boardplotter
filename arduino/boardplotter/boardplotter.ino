#include "plotter_drivers.h"
#include <Servo.h>
//#include "defs.h"

#define C_DOWN 2
#define C_MOVE 3
#define C_KICK 4
#define C_ENABLE 5
#define C_DISABLE 6

#define C_PEN0 7
#define C_PEN1 8
#define C_PEN2 9
#define PEN_DELAY 800

using namespace plotter_drivers;

Servo servo; 
int selectedServo = 0;
motor_conf motor1 { 2, 3, 7, 8, 6, true };
motor_conf motor2 { 4, 5 , 7, 8, 6, true };
servo_conf servo1 {1,1,1};
comdef commands[150];

easy_driver motors(&motor1, &motor2);

bool debug;
byte pen = -1;
void DEBUG(String s) {
  if (debug) {
    Serial.println(s);
  } 
}

void setup()
{
  debug = false;
  // pinMode(buttonPin, INPUT);
  Serial.begin(115200);
  motors.debug();
  
  motors.setStepSpeed(1);
  servo.attach(12);
  motors.disable();
  noPen();
  
  while (Serial.available() > 0) Serial.read();
}

void loop(){
  motors.enable();
  motors.step(10,10);
  delay(500);
  return;
  
  
  // empty buffer before ready
  //Serial.println("loop");
  
  String mode = "";
  
  // ready char
  Serial.println(">");
  while (Serial.available() == 0){ }; // wait for command

  String command = getCommand();
  DEBUG(command);
  
  if (command == "DO")
  {
    int i = 0;
    
    while(true) {
      
      String c = getCommand();      
      if (c == "END") {
        break;
      }
      commands[i] = getCommandDef(c);
      
      i++;
      if (i > 199)
        return;
    }

    for (int ci = 0; ci < i; ci++) {
      doCommand(commands[ci]);
    }
  }
  else {
    doCommand(getCommandDef(command));
  }
  //move(args);
}

comdef getCommandDef(String command) {
  comdef x;

  if (command =="KICK"){
    DEBUG("kick");
    x.command = C_KICK;
  }
 
  if(command == "PEN0")
    x.command = C_PEN0;
  if(command == "PEN1")
    x.command = C_PEN1;
  if(command == "PEN2")
    x.command = C_PEN2;
  
  if (command == "ENABLE")
    x.command = C_ENABLE;
    
  if (command == "DISABLE")
    x.command = C_DISABLE;
  
  if (command[0] == 'M') {
    x.command = C_MOVE;
    command.remove(0,1);
    x.data1 = getValue(command, ',',0).toInt();
    x.data2 = getValue(command, ',',1).toInt();
    x.data3 = getValue(command, ',',2).toInt();
  }
  return x;
}

void doCommand(comdef command) {
  
  DEBUG("C:" + String(command.command, DEC));
  if (command.command == C_KICK) {
    return;
  }
  
  if(command.command == C_PEN0)
    noPen();
  if(command.command == C_PEN1)
    pen1();
  if(command.command == C_PEN2)
    pen2();
  
  
  if (command.command == C_ENABLE)
    motors.enable();
  
  if (command.command == C_DISABLE)
    motors.disable();
  
  if (command.command == C_MOVE) {
    movemotors(command.data1, command.data2, command.data3);
  }
}

String getCommand() {
  while (Serial.available() == 0){ }; // wait for command
  String command = Serial.readStringUntil(';');
  //char c =   Serial.read(); // remove terminator char
  //Serial.println(c);
  Serial.println('<');
  return command;
}


void send_data(String data, String debugData){
  String out = "{\"data\":{" + data + "},\"debug\":{" + debugData + "}}";
  Serial.println(out);
}

void set_debug(String args) {
  if (args.equals("true"))
    debug = true;
  else if (args.equals("false"))
    debug = false;
  else {
    DEBUG("debug: args error");
  }
}

void movemotors(int a_steps, int b_steps, int speed) {
  motors.setStepSpeed(speed);
  motors.step((long)a_steps, (long)b_steps);
}

void move(String args) {
  int a_steps = getValue(args, ',',0).toInt();
  int b_steps = getValue(args, ',',1).toInt();
  int speed = getValue(args, ',',2).toInt();
  
  motors.setStepSpeed(speed);
  motors.step((long)a_steps, (long)b_steps);
  
}

void pen1() {
  if (pen != 1) {
    servo.write(50);
    delay(PEN_DELAY);
    pen = 1;
  }
  
}

void pen2() {
  if (pen != 2) {
    servo.write(110); 
    delay(PEN_DELAY);
    pen = 2;
  }
}


void noPen() {
  if (pen != 0) {
    servo.write(80);
    delay(PEN_DELAY);
    pen = 0;
  }
}


String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = { 
    0, -1   };
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }
  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}



