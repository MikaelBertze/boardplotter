
#include <Arduino.h>
#include "defs.h"

namespace plotter_drivers 
{
  class easy_driver {
    public:
      easy_driver(motor_conf *motor1, motor_conf *motor2);
      void enable();
      void disable();
      void setStepSpeed(int delay);    // delay between motor steps
      void step(int m1, int m2);
      void setupMotors();
      void debug();
        
    private:
      void reset(motor_conf *motor);
      void enableMotor(motor_conf *motor);
      void disableMotor(motor_conf *motor);
      void setDirection(motor_conf *motor, bool CW);
      void stepOne(bool motor1, bool motor2);
      
      int stepSleep = 5;    // Step duration in ms
      motor_conf *motor1;
      motor_conf *motor2;
  };
}