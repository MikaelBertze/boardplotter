#ifndef Stepper_h
#define Stepper_h

class StepperMotor {
public:
    StepperMotor(int m1In1, int m1In2, int m1In3, int m1In4, int m2In1, int m2In2, int m2In3, int m2In4);    // Constructor that will set the inputs
    void setStepDuration(int duration);    // delay between motor steps
    void step(long motorstep[], long result[]);

private:
    int duration;    // Step duration in ms
    int inputPins[4][4];  // The input pin numbers
};

#endif

