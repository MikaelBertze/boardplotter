#include <Arduino.h>
#include <StepperMotorDual.h>

StepperMotor::StepperMotor(int m1In1, int m1In2, int m1In3, int m1In4, int m2In1, int m2In2, int m2In3, int m2In4){
  // Record pin numbers in the inputPins array
  this->inputPins[0][0] = m1In1;
  this->inputPins[0][1] = m1In2;
  this->inputPins[0][2] = m1In3;
  this->inputPins[0][3] = m1In4;
  
  this->inputPins[1][0] = m2In1;
  this->inputPins[1][1] = m2In2;
  this->inputPins[1][2] = m2In3;
  this->inputPins[1][3] = m2In4;

  // Iterate through the inputPins array, setting each one to output mode
  for (int motor = 0; motor < 2; motor++) {
    for(int inputCount = 0; inputCount < 4; inputCount++){
      pinMode(this->inputPins[motor][inputCount], OUTPUT);
    }
  }
}

void StepperMotor::setStepDuration(int duration){
  this->duration = duration;
}

void StepperMotor::step(long motorstep[], long result[]){
  /*
      The following 2D array represents the sequence that must be
      used to acheive rotation. The rows correspond to each step, and
      the columns correspond to each input. L
  */
  bool sequence[][4] = {{LOW, LOW, LOW, HIGH },
                        {LOW, LOW, HIGH, HIGH},
                        {LOW, LOW, HIGH, LOW },
                        {LOW, HIGH, HIGH, LOW},
                        {LOW, HIGH, LOW, LOW },
                        {HIGH, HIGH, LOW, LOW},
                        {HIGH, LOW, LOW, LOW },
                        {HIGH, LOW, LOW, HIGH}};

  result[0] = 0;
  result[1] = 0;
  float divider[2] = {1.0,1.0};
  //int step[2] = {0,0};
  bool direction[2] = {motorstep[0] >= 0 , motorstep[1] >= 0};
  long numSteps = abs(motorstep[0]);

  if (abs(motorstep[0]) < abs(motorstep[1])) {
    divider[0] = abs((float)motorstep[0] / (float)motorstep[1]);
    numSteps = abs(motorstep[1]);
  }

  if (abs(motorstep[1]) < abs(motorstep[0])) {
    divider[1] = abs((float)motorstep[1] / (float)motorstep[0]);
    numSteps = abs(motorstep[0]);
  }
  
  // for (int i = 0; i < 2; i++) {
  //   if (motorstep[i] != 0) {
  //     step[i] = motorstep[i] / abs(motorstep[i]); 
  //   }
  // }
  
  int lastmove[] = {0, 0}; 
  float mpos[] = {0.0, 0.0};
  
  for (int i = 0 ; i < numSteps; i++) {
   	bool stepMotor[2] = {false, false};
    for (int motor = 0; motor < 2; motor++) {
   		int current = (int)mpos[motor];
   		mpos[motor] += divider[motor];
   		if ((int)mpos[motor] > current) {
   			// step one revolution
        stepMotor[motor] = true;
      }         
    }
    
    for (int step = 0; step < 8; step++) {
      for (int motor = 0; motor < 2; motor++) {
        if (stepMotor[motor]) {
          result[motor]++;
          int s = direction[motor] ? step : 7 - step;			
          for(int inputCount = 0; inputCount < 4; inputCount++) {
            digitalWrite(this->inputPins[motor][inputCount], sequence[s][inputCount]);
          }
        }
      }
      delayMicroseconds(this->duration);
   	}
  }   

   // shut down motors
  // for(int inputCount = 0; inputCount < 4; inputCount++) {
  //   digitalWrite(this->inputPins[0][inputCount], LOW);
  //   digitalWrite(this->inputPins[1][inputCount], LOW);
  // }  
}

