#include <Arduino.h>
#include <PlotterDriver.h>

PlotterDriver::PlotterDriver(int motor1[], int motor2[]){
  // Record pin numbers in the inputPins array
  this->motor1 = motor1;
  this->motor2 = motor2;

  this->setPinMode(this->motor1);
  this->setPinMode(this->motor2);
}

void PlotterDriver::setPinMode(int motor[]) {
  pinMode(motor[this->stp], OUTPUT);
  pinMode(motor[this->dir], OUTPUT);
  pinMode(motor[this->MS1], OUTPUT);
  pinMode(motor[this->MS2], OUTPUT);
  pinMode(motor[this->EN], OUTPUT);
  this->reset(motor);
}

void PlotterDriver::reset(int motor[]) {
  digitalWrite(motor[this->stp], LOW);
  digitalWrite(motor[this->dir], LOW);
  digitalWrite(motor[this->MS1], LOW);
  digitalWrite(motor[this->MS2], LOW);
  digitalWrite(motor[this->EN], HIGH);
}

void PlotterDriver::enable() {
  digitalWrite(this->motor1[this->EN], LOW);
  digitalWrite(this->motor2[this->EN], LOW);
}

void PlotterDriver::disable() {
  digitalWrite(this->motor1[this->EN], HIGH);
  digitalWrite(this->motor2[this->EN], HIGH);
}



void PlotterDriver::setStepSpeed(int stepSleep){
  this->stepSleep = stepSleep;
}

void PlotterDriver::stepOne(bool motor1, bool motor2) {
  digitalWrite(this->motor1[this->stp], motor1 ? HIGH : LOW); //Trigger one step
  digitalWrite(this->motor2[this->stp], motor2 ? HIGH : LOW); //Trigger one step
  delay(1);
  digitalWrite(this->motor1[this->stp], LOW); //Pull step pin low so it can be triggered again
  digitalWrite(this->motor2[this->stp], LOW); //Pull step pin low so it can be triggered again
  delay(this->stepSleep);
}

void PlotterDriver::setDirection(int motor[], bool CW) {
  digitalWrite(motor[this->dir], CW ? HIGH : LOW);
  
}

void PlotterDriver::step(int motor1, int motor2){
  
  int moves1 = 0;
  int moves2 = 0;

  float motor1divider = 1.0;
  float motor2divider = 1.0;
  //int step[2] = {0,0};
  bool dir1 = motor1 > 0;
  bool dir2 = motor2 > 0;
  this->setDirection(this->motor1, dir1);
  this->setDirection(this->motor2, dir2);
  //delay(1);

  int motor2steps = abs(motor2);
  int motor1steps = abs(motor1);

  int numSteps = motor1steps;

  if (motor1steps < motor2steps) {
    motor1divider = (float)motor1steps / motor2steps;
    numSteps = motor2steps;
  }

  if (motor2steps < motor1steps) {
    motor2divider = (float)motor2steps / motor1steps;
    numSteps = motor1steps;
  }
  
  float motor1current = .5;
  float motor2current = .5;

  for (int i = 0 ; i < numSteps; i++) {
   	
     int current1 = (int)motor1current;
     int current2 = (int)motor2current;
     
     motor1current += motor1divider;
     motor2current += motor2divider;

     bool move1 = (int)motor1current != current1;
     bool move2 = (int)motor2current != current2;
     
     if (move1 > motor1steps)
      move1 = false;
     if (move2 > motor2steps)
      move1 = false;
     
     this->stepOne(move1, move2);
     moves1 += move1 ? 1 : 0;
     moves2 += move2 ? 1 : 0;
  }

  while (moves1 < motor1steps) {
    this->stepOne(true, false);
    moves1++;
  }

  while (moves2 < motor2steps) {
    this->stepOne(false, true);
    moves2++;
  }
}   

   // shut down motors
  // for(int inputCount = 0; inputCount < 4; inputCount++) {
  //   digitalWrite(this->inputPins[0][inputCount], LOW);
  //   digitalWrite(this->inputPins[1][inputCount], LOW);
  // }  



