#ifndef Stepper_h
#define Stepper_h

class PlotterDriver {
public:
    PlotterDriver(int motor1[], int motor2[]);
    void enable();
    void disable();
    void setStepSpeed(int delay);    // delay between motor steps
    void step(int m1, int m2);

private:
    void setPinMode(int motor[]);
    void reset(int motor[]);
    void stepOne(bool motor1, bool motor2);
    void setDirection(int motor[], bool CW);
    int stepSleep = 5;    // Step duration in ms
    int * motor1;
    int * motor2;

    int stp = 0;
    int dir = 1;
    int MS1 = 2;
    int MS2 = 3;
    int EN = 4;

};

#endif

